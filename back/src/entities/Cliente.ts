import { BaseEntity, Column, Entity, OneToMany, PrimaryColumn } from "typeorm";
import { Historico } from "./Historico";

@Entity()
export class Cliente extends BaseEntity {
    @PrimaryColumn()
    rut: string

    @Column()
    nombre: string

    @Column()
    apellido: string

    @Column()
    cuenta: string

    @Column()
    saldo: number

    @Column('tinytext')
    estado: string

    @OneToMany(() => Historico, (historico) => historico.cliente)
    historicos: Historico[]
}