import { Historico } from './Historico';
import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Producto extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: string

    @Column()
    descripcion: string

    @Column()
    precio: number

    @OneToMany(() => Historico, historico => historico.producto)
    historicos: Historico[]
}