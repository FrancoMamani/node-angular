import { Producto } from './Producto';
import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Cliente } from "./Cliente";

@Entity()
export class Historico extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: string

    @Column()
    descripcion: string

    @Column()
    recarga: number

    @Column()
    compra: number

    @Column()
    categoria: string

    @ManyToOne(() => Cliente, (cliente) => cliente.historicos)
    cliente: Cliente

    @ManyToOne(() => Producto, (producto) => producto.historicos)
    producto: Producto
}