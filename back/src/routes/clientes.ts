import express from 'express'
import { agregarSaldo, comprarProducto, editarNombre, obtenerCliente, obtenerClientes } from '../controllers/clientes'

const router = express.Router()

router.get('/obtenerClientes', obtenerClientes)
router.get('/obtenerCliente/:rut', obtenerCliente)
router.put('/agregarSaldo/:rut', agregarSaldo)
router.put('/comprarProducto/:rut', comprarProducto)
router.put('/editarNombre/:rut', editarNombre)

export = router