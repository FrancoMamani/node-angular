import express from 'express'
import { obtenerProductos } from '../controllers/productos'

const router = express.Router()

router.get('/obtenerProductos', obtenerProductos)

export = router