import express from 'express'
import fs from 'fs'

const router = express.Router()
const PATH_ROUTES = __dirname

const removeExtension = (fileName: string): string => fileName.split('.').shift() || fileName.split('.')[0]

fs.readdirSync(PATH_ROUTES).forEach(file => {
    const fileName = removeExtension(file)

    if(fileName !== 'index') {
        router.use(`/${fileName}`, require(`./${fileName}`))
    }
})

export = router
