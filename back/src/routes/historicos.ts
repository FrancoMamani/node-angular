import { obtenerHistoricos } from './../controllers/historicos';
import express from 'express'

const router = express.Router()

router.get('/obtenerHistoricos/:rut', obtenerHistoricos)

export = router