import 'dotenv/config'
import { DataSource } from 'typeorm'
import { Cliente } from '../entities/Cliente'
import { Historico } from '../entities/Historico'
import { Producto } from '../entities/Producto'

const appDataSource = new DataSource({ 
    type: "mysql",
    host: "localhost",
    port: parseInt(process.env.DB_PORT || "3306"),
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    entities: [Cliente, Historico, Producto],
    synchronize: true,
    logging: false,
})

export default appDataSource