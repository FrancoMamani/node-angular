import 'dotenv/config'
import 'reflect-metadata'

import app from './app'
import appDataSource from './config/data-source'

const SERVER_PORT = parseInt(process.env.SERVER_PORT || '3000')

app.listen(SERVER_PORT, () => {
    console.log(`El servidor esta corriendo en el puerto ${SERVER_PORT}`)
})

appDataSource.initialize()
    .then(() => {
        console.log("BASE DE DATOS CONECTADA CORRECTAMENTE")
    })
    .catch(error => console.log(error))


