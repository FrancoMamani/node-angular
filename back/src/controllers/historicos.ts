import { Request, Response } from "express";
import { Historico } from "../entities/Historico";


export const obtenerHistoricos = async (req: Request, res: Response) => {
    try {
        const {rut} = req.params
        const historicos: Historico[] = await Historico.findBy({cliente: {rut}})
        res.json(historicos)
    } catch (error) {
        res.json(error)
    }
}