import { Request, Response } from "express";
import { Cliente } from "../entities/Cliente";
import { Historico } from "../entities/Historico";
import { Producto } from "../entities/Producto";

export const obtenerClientes = async (req: Request, res: Response) => {
    try {
        const clientes: Cliente[] = await Cliente.find()
        res.json(clientes)
    
    } catch (error) {
        res.json(error)
    }
}

export const obtenerCliente = async (req: Request, res: Response) => {
    try {
        const {rut} = req.params
        const cliente: Cliente | null = await Cliente.findOneBy({rut}) 

        if(cliente != null){
            res.json(cliente)
        } else {
            throw `El rut ${rut} no fue encontrado`
        }
        
    } catch (error) {
        res.json({error})
    }
}

export const agregarSaldo = async (req: Request, res: Response) => {
    try {
        const {rut}= req.params
        const {recarga, nuevoSaldo} = req.body

        const cliente: Cliente | null = await Cliente.findOneBy({rut})
        
        if(cliente != null){
            const historico = new Historico()
            historico.descripcion = 'Recarga'
            historico.recarga     = recarga
            historico.compra      = 0
            historico.cliente     = cliente
            //En este caso la clave foranea del producto quedaria null al no ser una compra

            await Cliente.update({rut}, {'saldo': nuevoSaldo})
            await Historico.save(historico)

            res.status(200).json({'mensaje': 'El sueldo fue agregado correctamente'})
        }else{
            throw `El rut no fue encontrado`

        }
    } catch (error) {
        res.json(error)
    }
}

export const comprarProducto = async (req: Request, res: Response) => {
    try {
        const {rut}= req.params
        const {id, descripcion, compra, nuevoSaldo, categoria} = req.body

        const cliente: Cliente | null = await Cliente.findOneBy({rut})
        const producto: Producto | null = await Producto.findOneBy({id})
        
        if(cliente != null && producto != null){
            const historico = new Historico()
            historico.descripcion = descripcion
            historico.recarga     = 0
            historico.compra      = compra
            historico.cliente     = cliente
            historico.producto    = producto
            historico.categoria   = categoria
            //En este caso la clave foranea del producto quedaria null al no ser una compra

            await Cliente.update({rut}, {'saldo': nuevoSaldo})
            await Historico.save(historico)

            res.status(200).json({'mensaje': 'La compra fue agregado correctamente'})
        } else {
            throw `el rut no fue encontrado`
        }
    } catch (error) {
        res.json(error)
    }
}

export const editarNombre = async (req: Request, res: Response) => {
    try {
        const {rut} = req.params
        const {nuevoNombre} = req.body
        await Cliente.update({rut}, {nombre: nuevoNombre})
    } catch (error) {
        res.json(error)
    }
}