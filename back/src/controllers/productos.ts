import { Request, Response } from "express";

import { Producto } from "../entities/Producto";


export const obtenerProductos = async (req: Request, res: Response) => {
    try {
        const productos: Producto[] = await Producto.find()
        res.json(productos)
    } catch (error) {
        res.json(error)
    }
}