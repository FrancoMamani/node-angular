export interface Historico {
    id: number
    descripcion: string
    recarga: number
    compra: number
    categoria: string
}