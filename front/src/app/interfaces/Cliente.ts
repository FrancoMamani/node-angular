export interface Cliente {
    rut: string
    nombre: string
    apellido: string
    cuenta: string
    saldo: number
    estado: string
}
