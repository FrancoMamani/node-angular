import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cliente } from '../interfaces/Cliente';
import { Producto } from '../interfaces/Producto';
import { Historico } from '../interfaces/Historico';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  baseURL: string = 'http://localhost:3000/api'

  constructor(private http: HttpClient) { }

  obtenerClientes(): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(`${this.baseURL}/clientes/obtenerClientes`)
  }

  obtenerCliente(rut: string | null): Observable<Cliente> {
    return this.http.get<Cliente>(`${this.baseURL}/clientes/obtenerCliente/${rut}`)
  }

  obtenerProductos(): Observable<Producto[]> {
    return this.http.get<Producto[]>(`${this.baseURL}/productos/obtenerProductos`)
  }

  agregarSaldo(rut: string | null, recarga: number, saldoActual: number): Observable<any> {
    return this.http.put(`${this.baseURL}/clientes/agregarSaldo/${rut}`, {
      recarga,
      nuevoSaldo: (saldoActual + recarga)
    })
  }

  comprarProducto(rut: string | null, id: number, descripcion: string, compra: number, saldoActual: number, categoria: any): Observable<any> {
    return this.http.put(`${this.baseURL}/clientes/comprarProducto/${rut}`, {
      id,
      descripcion,
      compra,
      nuevoSaldo: (saldoActual - compra),
      categoria
    })
  }


  editarNombre(rut: string, nuevoNombre: string | null): Observable<any> {
    return this.http.put(`${this.baseURL}/clientes/editarNombre/${rut}`, {nuevoNombre})
  }

  obtenerHistoricos(rut: string | null): Observable<Historico[]> {
    return this.http.get<Historico[]>(`${this.baseURL}/historicos/obtenerHistoricos/${rut}`)
  }
}
