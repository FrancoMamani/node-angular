import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgregarSaldoComponent } from './pages/agregarSaldo/agregar-saldo.component';
import { ComprarProductoComponent } from './pages/comprarProducto/comprar-producto.component';
import { TablaComponent } from './pages/tabla/tabla.component';
import { VerHistoricosComponent } from './pages/verHistoricos/ver-historicos.component';

const routes: Routes = [
  {path: '', component: TablaComponent},
  {path: 'agregarSaldo/:rut', component: AgregarSaldoComponent},
  {path: 'comprarProducto/:rut', component: ComprarProductoComponent},
  {path: 'verHistoricos/:rut', component: VerHistoricosComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
