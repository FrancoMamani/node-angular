import { ClienteService } from 'src/app/services/cliente.service';
import { Component } from '@angular/core';
import { Producto } from 'src/app/interfaces/Producto';
import { ActivatedRoute, Router } from '@angular/router';
import { Cliente } from 'src/app/interfaces/Cliente';

@Component({
  selector: 'app-comprar-producto',
  templateUrl: './comprar-producto.component.html',
  styleUrls: ['./comprar-producto.component.css']
})
export class ComprarProductoComponent {
  productos: Producto[] = []
  cliente: Cliente | undefined
  rut_ruta: string | null = ''
  
  producto: Producto | undefined
  precio : number = 0
  descripcion: string = ''

  categoria: any

  constructor(private clienteService: ClienteService, private route: ActivatedRoute, private router: Router) {
    this.rut_ruta = this.route.snapshot.paramMap.get('rut')
    
    this.clienteService.obtenerProductos().subscribe((producto: Producto[]) => {
      this.productos = producto
    })

    this.clienteService.obtenerCliente(this.rut_ruta).subscribe( (cliente: Cliente) => {
      this.cliente = cliente      
    })
  }

  productoSeleccionado(producto: any): void {
      this.producto = producto
      this.precio = this.producto?.precio || 0
      this.descripcion = this.producto?.descripcion || ''
  }

  comprarProducto(): void {
    this.clienteService.comprarProducto(
      this.rut_ruta, this.producto?.id || 0, this.descripcion, this.precio, (this.cliente?.saldo || 0), this.categoria)
        .subscribe()
        
    alert(`Se ha comprado ${this.descripcion}`)
    window.location.reload();
  }


  volverAtras(): void {
    this.router.navigate(['/']);
  }
  
}
