import { Component } from '@angular/core';
import { Cliente } from 'src/app/interfaces/Cliente';
import { ClienteService } from 'src/app/services/cliente.service';

@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.css']
})
export class TablaComponent {
  clientes: Cliente[] = []

  constructor(private clienteService: ClienteService) {
    this.clienteService.obtenerClientes().subscribe((clientes: Cliente[]) => {
      this.clientes = clientes
    })
  }


  editarNombre(rut: string): void {
    let nuevoNombre = prompt('Ingrese su nuevo nombre: ')

    if(nuevoNombre != '' && nuevoNombre !== null) {
      this.clienteService.editarNombre(rut, nuevoNombre).subscribe()
      alert('Se ha cambiado el nombre exitosamente')
      window.location.reload();
    }else{
      alert('ERROR AL INGRESAR NUEVO NOMBRE')
    }
  }
}
