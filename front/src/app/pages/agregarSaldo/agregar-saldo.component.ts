import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClienteService } from 'src/app/services/cliente.service';
import { Cliente } from 'src/app/interfaces/Cliente';

@Component({
  selector: 'app-agregar-saldo',
  templateUrl: './agregar-saldo.component.html',
  styleUrls: ['./agregar-saldo.component.css']
})
export class AgregarSaldoComponent {
  cliente:Cliente | undefined
  recarga: number = 0
  rut_ruta: string | null

  constructor(private clienteService: ClienteService, private route: ActivatedRoute, private router: Router) {
    this.rut_ruta = this.route.snapshot.paramMap.get('rut')
    this.clienteService.obtenerCliente(this.rut_ruta).subscribe( (cliente: Cliente) => {
      this.cliente = cliente      
    })
  }

  agregarSaldo(): void {
    this.clienteService.agregarSaldo(this.rut_ruta, this.recarga, (this.cliente?.saldo || 0)).subscribe()
    alert('Recargado')
    window.location.reload();
  }

  volverAtras(): void {
    this.router.navigate(['/']);
  }
}
