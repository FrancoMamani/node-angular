import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarSaldoComponent } from './agregar-saldo.component';

describe('AgregarSaldoComponent', () => {
  let component: AgregarSaldoComponent;
  let fixture: ComponentFixture<AgregarSaldoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarSaldoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AgregarSaldoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
