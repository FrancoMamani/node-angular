import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cliente } from 'src/app/interfaces/Cliente';
import { Historico } from 'src/app/interfaces/Historico';
import { ClienteService } from 'src/app/services/cliente.service';

@Component({
  selector: 'app-ver-historicos',
  templateUrl: './ver-historicos.component.html',
  styleUrls: ['./ver-historicos.component.css']
})
export class VerHistoricosComponent {
  historicos: Historico[] = []
  rut_ruta: string | null

  total_recarga: number = 0
  total_compra: number = 0

  constructor(private clienteService: ClienteService, private route: ActivatedRoute, private router: Router) {
    this.rut_ruta = this.route.snapshot.paramMap.get('rut')
    this.clienteService.obtenerHistoricos(this.rut_ruta).subscribe( (historicos: Historico[]) => {
      this.historicos = historicos
      
      this.total_recarga = historicos.reduce((total, objeto) => total + objeto.recarga, 0)
      this.total_compra = historicos.reduce((total, objeto) => total + objeto.compra, 0)
    })
  }


  volverAtras(): void {
    this.router.navigate(['/']);
  }
}
