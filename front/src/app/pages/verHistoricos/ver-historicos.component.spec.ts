import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerHistoricosComponent } from './ver-historicos.component';

describe('VerHistoricosComponent', () => {
  let component: VerHistoricosComponent;
  let fixture: ComponentFixture<VerHistoricosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerHistoricosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VerHistoricosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
