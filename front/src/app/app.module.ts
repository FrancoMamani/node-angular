import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TablaComponent } from './pages/tabla/tabla.component';
import { HttpClientModule } from '@angular/common/http';
import { AgregarSaldoComponent } from './pages/agregarSaldo/agregar-saldo.component';
import { FormsModule } from '@angular/forms';
import { ComprarProductoComponent } from './pages/comprarProducto/comprar-producto.component';
import { VerHistoricosComponent } from './pages/verHistoricos/ver-historicos.component';

@NgModule({
  declarations: [
    AppComponent,
    TablaComponent,
    AgregarSaldoComponent,
    ComprarProductoComponent,
    VerHistoricosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
